@extends('adminlte.master')

@section('judulFile')
  Halaman Selamat Datang
@endsection

@section('judul')
  Selamat Datang
@endsection

@section('isi')
    Selamat datang, silahkan memilih halaman yang anda inginkan
    <br>
    <a href="/cast">Halaman Table Cast</a>
    <br>
    <a href="/cast/create">Halaman Tambah Cast</a>
@endsection
