@extends('adminlte.master')

@section('isi')
    <div class="mt-3 ml-3">
        <h4>Nama : {{$cast->nama}}</h4>
        <h5>Umur : {{$cast->umur}}</h5>
        <p>Bio : {{$cast->bio}}</p>
    </div>

@endsection